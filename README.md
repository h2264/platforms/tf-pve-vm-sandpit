# tf-pve-vm-sandpit

Sandpit repository used for testing application workloads in a single node.

## Assumptions
- [x] Proxmox remote host configured.
- [x] Localhost has OCI technology installed. E.g. docker, podman.
- [x] ssh public-private keypair set.

## Systemprep
```bash
# Clone repository along with its git submodules
git clone --recursive git@gitlab.com:h2264/platforms/tf-pve-vm-sandpit.git
cd tf-pve-vm-focal

# Initialise terraform repository.
make terraform-init

# Configure terraform payload. Initialise the variables.
#   pve_host: IPv4 of Proxmox remote host.
#   pve_user: Proxmox service account ID
#   pve_password: Proxmox service account password
#   template_password: Deployment password
#   template_sshpubkey: Deployment ssh public key
cat << 'EOF' >> ./src/payload.tfvars
pve_host= 
pve_user=
pve_password=
template_password=
template_sshpubkey=
EOF

# Test configuration
make terraform-test
```

## Deployment
```bash
# Implement configuration. Stdout used of CICD integration.
# STDOUT:
#   instance_domainname : list of domain names
#   instance_hostname   : list of host names
#   instance_ipv4_addr  : list of IPv4 addresses
make terraform-apply

# Destroy configuration
make terraform-clean
```

## CICD integration
### System prep
### Repository Layout
### Payloads
### Usage
