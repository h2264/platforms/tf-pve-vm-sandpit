# include base makefile
include .make/base.mk

# include terraform support
include .make/terraform.mk

# include override residing in current repository
-include Override.mk
