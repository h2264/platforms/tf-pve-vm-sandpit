variable "pve_host" {
    type = string
}

variable "pve_password" {
    type = string
    sensitive   = true
}
variable "pve_user" {
    type = string
    default = "root"
}

variable "subnet_nameserver" {
    type    = string
    default = "10.1.1.140"
}

variable "subnet_domain" {
    type    = list(string)
    default = [
        "fwc.dc.dev.apps.vm",
    ]
}

variable "vm_count" {
    type = number
    default = 1
}

variable "vlan" {
    type = list(number)
    default = [
        4
    ]
}

variable "template_host_prefix" {
    type = list(string)
    default = [
        "sp-dev-focal"
    ]
}

variable "template_host_description" {
    type = string
    default = "Deployed using terraform"
}

variable "template_image" {
    type = string
    default = "template-focal-server-cloudimg-amd64.img"
}

variable "template_host_cores" {
    type = number
    default = 2
}

variable "template_host_sockets" {
    type = number
    default = 2
}

variable "template_host_memory" {
    type = number
    default = 4096
}


# STORAGE
variable "template_disk_size" {
    type = string
    default = "9420M"
}
variable "template_disk_type" {
    type = string
    default = "scsi"
}
variable "template_disk_storage" {
    type = string
    default = "ceph_rbd"
}


# Cloud init configuration
variable "template_user" {
    type = string
    default = "cloudy"
}

variable "template_password" {
    type = string
    sensitive   = true
}

variable "template_sshpubkey" {
    type = string
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCh+9w3QKfIsnNxWVqT02YkxJlkOWRxuyXkvaVM8VD3vOfVAe/fd/YERaAHiygpsbdXbnwkzMAjayNHV9U1bwKk2u57rSa4Cr0CPKwuz0B/J4MGJ50eYNTB44uFL00Ub2l+a5ZqQ6YAB+/3BB6Y4CYYqcTlc3uCTx34vwaPo1qoI1X5H05g4+fM4WTzZBEth5lI2zvs4P3si0H1O1+bq52c/L9lCjfU0k34h8jgZsEt7ueU4BmRFMQJaNehykU6GLnqD07bAEwGANmbiXiFyXVuWKU10KjHt1dOlFsjQBbtxFcKT+fWjkNFI38Gme2zCYvdBj6pFSuz7K6UzJ0j7ixIe8n2LmfjEJt1FFgvOStyuIF4F29xFoQ4GX2yCbYA0lrRmWO+YTnejWHtnZ012bJezoEhd+BrVX+HiJ8G6ClemL9TAoRCiC1GLoPEdkZ651Nf89WHmKqvxcCPCGxIdRA6JriE0iARVsN4tm0uzrlNX/X8jMnMHTzWZLqjKRjF38M= bootstrapp@homelab"
}