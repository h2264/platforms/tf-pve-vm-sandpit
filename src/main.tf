terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
    }
  }
}

provider "proxmox" {
    pm_tls_insecure = true
    pm_api_url      = "https://${var.pve_host}:8006/api2/json"
    pm_password     = var.pve_password
    pm_user         = "${var.pve_user}@pam"
    pm_debug        = true
    pm_timeout      = 900
    pm_log_levels   = {
        _default    = "debug"
        _capturelog = ""
    }
}

/* Null resource that generates a cloud-config file per vm */
data "template_file" "user_data" {
  count    = var.vm_count
  template = file("${path.module}/templates/user_data.cfg")
  vars = {
    hostname                  = "${var.template_host_prefix[count.index]}-${count.index}"
    fqdn                      = "${var.template_host_prefix[count.index]}-${count.index}.${var.subnet_domain[count.index]}"
    user                      = var.template_user
    password                  = var.template_password
    ssh_authorized_key        = var.template_sshpubkey
    nameserver_ipv4           = var.subnet_nameserver
    nameserver_search_domain  = var.subnet_domain[count.index]
  }
}

resource "local_file" "cloud_init_user_data_file" {
  count    = var.vm_count
  content  = data.template_file.user_data[count.index].rendered
  filename = "${path.module}/files/user_data-${var.template_host_prefix[count.index]}-${count.index}.cfg"
}

resource "null_resource" "cloud_init_config_files" {
  count = var.vm_count
  depends_on = [
      local_file.cloud_init_user_data_file,
  ]

  connection {
    type     = "ssh"
    user     = var.pve_user
    password = var.pve_password
    host     = var.pve_host
  }

  provisioner "file" {
    source      = "${path.module}/files/user_data-${var.template_host_prefix[count.index]}-${count.index}.cfg"
    destination = "/mnt/pve/synology/snippets/user_data-${var.template_host_prefix[count.index]}-${count.index}.cfg"
  }
}


resource "proxmox_vm_qemu" "pve-template-test" {
    count = var.vm_count
    depends_on = [
        null_resource.cloud_init_config_files,
    ]

    target_node = "loki"
    pool = "operations"
    clone = var.template_image

    name = "${var.template_host_prefix[count.index]}-${count.index}"
    desc = var.template_host_description

    agent     = 1 # Enable QEMU guest agent by default to determine host IP
    cores     = var.template_host_cores
    sockets   = var.template_host_sockets
    memory    = var.template_host_memory
    oncreate  = true

    disk {
        size            = var.template_disk_size
        type            = var.template_disk_type
        storage         = var.template_disk_storage
        # storage_type = "zfs"
    }

    
    dynamic "network" {
      iterator = vlan_tag
      for_each = var.vlan
      content {
          model = "virtio"
          bridge = "vmbr0"
          tag = vlan_tag.value
      }
    }

    # Ignore changes to the network
    ## MAC address is generated on every apply, causing
    ## TF to think this needs to be rebuilt on every apply
    lifecycle {
      ignore_changes = [
        network
      ]
    }

    bootdisk      = "scsi0"
    os_type       = "cloud-init"
    scsihw        = "virtio-scsi-pci"
    # cloud init
    # ci_wait       = var.ci_wait
    ciuser        = var.template_user
    # cipassword    = "cloudy"
    cicustom      = "user=synology:snippets/user_data-${var.template_host_prefix[count.index]}-${count.index}.cfg"
    cloudinit_cdrom_storage = var.template_disk_storage
    nameserver    = var.subnet_nameserver
    searchdomain  = var.subnet_domain[count.index]
    ipconfig0     = "ip=dhcp"

    timeouts {
      create = "60m"
      delete = "2h"
    }
}

output "instance_hostname" {
  depends_on = [
    proxmox_vm_qemu.pve-template-test
  ]
  value = [proxmox_vm_qemu.pve-template-test.*.name]
}
output "instance_domainname" {
  depends_on = [
    proxmox_vm_qemu.pve-template-test
  ]
  value = [proxmox_vm_qemu.pve-template-test.*.searchdomain]
}


output "instance_ipv4_addr" {
  depends_on = [
    proxmox_vm_qemu.pve-template-test
  ]
  value = [proxmox_vm_qemu.pve-template-test.*.ssh_host]
}